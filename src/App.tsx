import React from 'react';

import {useLoad} from './hooks/service';

import {BLACK_FOREST} from './constants';

function App() {
  const [data, poll] = useLoad('OWM', BLACK_FOREST);

  React.useEffect(() => {
    poll();
  }, [poll]);

  return (
    <pre className="App">
      {JSON.stringify(data,null,2)}
    </pre>
  );
}

export default App;
