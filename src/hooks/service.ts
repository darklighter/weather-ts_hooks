import * as React from 'react';

import * as OWM from '../provider/OpenWeatherMap';


const apiFetch = (url: string) => ( fetch(url).then(res => res.json()) );

export const useLoad = (provider: string, id: number) => {
  const [data, setData] = React.useState();

  const url = OWM.weeks({
    id: id,
    units: 'imperial',
  });

  const poll = React.useCallback(
    () => apiFetch(url).then(x => setData(x)),
    [url, setData]
  );

  return [ data, poll ];
};
