# OpenWeatherMap output examples

The default return value for OWM is JSON (XML can be supplied by passing
`mode=XML`).

## Current

```
{
  "coord": {
    "lon": -104.7,
    "lat": 39.01
  },
  "weather": [
    {
      "id": 802,
      "main": "Clouds",
      "description": "scattered clouds",
      "icon": "03d"
    }
  ],
  "base": "stations",
  "main": {
    "temp": 38.28,
    "feels_like": 29.3,
    "temp_min": 30,
    "temp_max": 46,
    "pressure": 1015,
    "humidity": 32
  },
  "visibility": 16093,
  "wind": {
    "speed": 5.82,
    "deg": 50
  },
  "clouds": {
    "all": 40
  },
  "dt": 1586905937,
  "sys": {
    "type": 1,
    "id": 3234,
    "country": "US",
    "sunrise": 1586866953,
    "sunset": 1586914490
  },
  "timezone": -21600,
  "id": 5414069,
  "name": "Black Forest",
  "cod": 200
}
```

## Near forecast
(_note that_ `list` _has been truncated by_ `cnt - 2` _entries_)
```
{
  "cod": "200",
  "message": 0,
  "cnt": 40,
  "city": {
    "id": 5414069,
    "name": "Black Forest",
    "coord": {
      "lat": 39.0131,
      "lon": -104.7008
    },
    "country": "US",
    "timezone": -21600,
    "sunrise": 1586953266,
    "sunset": 1587000949
  },
  "list": [
    {
      "dt": 1586962800,
      "main": {
        "temp": 41.92,
        "feels_like": 31.26,
        "temp_min": 33.12,
        "temp_max": 41.92,
        "pressure": 1016,
        "sea_level": 1016,
        "grnd_level": 776,
        "humidity": 31,
        "temp_kf": 4.89
      },
      "weather": [
        {
          "id": 803,
          "main": "Clouds",
          "description": "broken clouds",
          "icon": "04d"
        }
      ],
      "clouds": {
        "all": 72
      },
      "wind": {
        "speed": 9.08,
        "deg": 241
      },
      "sys": {
        "pod": "d"
      },
      "dt_txt": "2020-04-15 15:00:00"
    },
    {
      "dt": 1586973600,
      "main": {
        "temp": 46.09,
        "feels_like": 35.04,
        "temp_min": 39.49,
        "temp_max": 46.09,
        "pressure": 1012,
        "sea_level": 1012,
        "grnd_level": 775,
        "humidity": 25,
        "temp_kf": 3.67
      },
      "weather": [
        {
          "id": 803,
          "main": "Clouds",
          "description": "broken clouds",
          "icon": "04d"
        }
      ],
      "clouds": {
        "all": 77
      },
      "wind": {
        "speed": 9.64,
        "deg": 247
      },
      "sys": {
        "pod": "d"
      },
      "dt_txt": "2020-04-15 18:00:00"
    },
  ]
}
```

## Far foecast
```

```
