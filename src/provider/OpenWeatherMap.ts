// import * as React from 'react';

import * as qs from 'querystring';
import * as keys from './OpenWeatherMap.keys';

const api = 'https://api.openweathermap.org/data/2.5/';
const key = { appid: keys.appid };
const requests = {
  current: 'weather',
  days: 'forecast',
  weeks: 'forecast/daily'
};

const getURL = (future: string, params: object) => {
  const prm = Object.assign({}, key, params);

  return `${api}${future}?${qs.stringify(prm)}`;
};

/*
 *  params should look like one of :
 *    { id: <integer> },
 *    { lat: <float>, lon: <float> },
 *    { q: 'city[,state[,country_code]]' },
 *    { zip: '<float>,<country_code>' }
 */

export const current = (params: object) => getURL(requests.current, params);
export const days = (params: object) => getURL(requests.days, params);
export const weeks = (params: object) => getURL(requests.weeks, params);
